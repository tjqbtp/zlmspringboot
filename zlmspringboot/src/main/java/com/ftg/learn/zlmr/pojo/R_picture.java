package com.ftg.learn.zlmr.pojo;

/**
 * @author TJQ
 * @data 2019/10/12 8:46
 * 轮播图片表
 */
public class R_picture {

    private String r_p_id; // 图片id
    private String r_p_name; // 图片名
    private String r_p_url; // 图片路径
    private String r_p_remarks; // 备注

    public String getR_p_id() {
        return r_p_id;
    }

    public void setR_p_id(String r_p_id) {
        this.r_p_id = r_p_id;
    }

    public String getR_p_name() {
        return r_p_name;
    }

    public void setR_p_name(String r_p_name) {
        this.r_p_name = r_p_name;
    }

    public String getR_p_url() {
        return r_p_url;
    }

    public void setR_p_url(String r_p_url) {
        this.r_p_url = r_p_url;
    }

    public String getR_p_remarks() {
        return r_p_remarks;
    }

    public void setR_p_remarks(String r_p_remarks) {
        this.r_p_remarks = r_p_remarks;
    }
}
