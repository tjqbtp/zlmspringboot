package com.ftg.learn.zlmr.pojo;

import org.springframework.stereotype.Repository;

@Repository
public class R_feedinfor {
    private String phone;
    private String email;
    private String title;
    private String text;


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
