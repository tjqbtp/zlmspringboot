package com.ftg.learn.zlmr.mapper;

import com.ftg.learn.zlmr.pojo.R_picture;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/12 9:02
 */
@Mapper
public interface R_pictureMapper {

    @Select("select * from r_picture")
    public List<R_picture> showRpAll();

}
