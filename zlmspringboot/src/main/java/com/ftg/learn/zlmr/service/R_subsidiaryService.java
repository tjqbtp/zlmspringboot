package com.ftg.learn.zlmr.service;

import com.ftg.learn.zlmr.mapper.R_subsidiaryMapper;
import com.ftg.learn.zlmr.pojo.R_dynamic;
import com.ftg.learn.zlmr.pojo.R_subsidiary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/11 16:45
 */
@Service
public class R_subsidiaryService {

    @Autowired
    R_subsidiaryMapper rsm;

    public List<R_subsidiary> showRsAll(String curr, String limit){
        return rsm.showRsAll((Integer.valueOf(curr)-1)*Integer.valueOf(limit),Integer.valueOf(limit));
    }

    public int showRsCount(){return rsm.showRsCount();}

}
