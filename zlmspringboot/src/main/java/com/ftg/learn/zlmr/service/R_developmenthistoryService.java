package com.ftg.learn.zlmr.service;

import com.ftg.learn.zlmr.mapper.R_developmenthistoryMapper;
import com.ftg.learn.zlmr.pojo.R_developmenthistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/11 16:44
 */
@Service
public class R_developmenthistoryService {

    @Autowired
    R_developmenthistoryMapper rdhm;

    public List<R_developmenthistory> showRdhAll(){
        return rdhm.showRdhAll();
    }

}
