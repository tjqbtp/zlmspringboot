package com.ftg.learn.zlmr.mapper;

import com.ftg.learn.zlmr.pojo.R_developmenthistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/11 16:40
 */
@Mapper
public interface R_developmenthistoryMapper {

    @Select("select * from r_developmenthistory order by r_dh_id asc")
    public List<R_developmenthistory> showRdhAll();

}
