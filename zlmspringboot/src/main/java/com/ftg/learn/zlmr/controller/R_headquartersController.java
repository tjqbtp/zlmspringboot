package com.ftg.learn.zlmr.controller;

import com.ftg.learn.zlmr.pojo.R_headquarters;
import com.ftg.learn.zlmr.service.R_headquartersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/11 16:47
 */
@RestController
@CrossOrigin
@RequestMapping("/rhq")
public class R_headquartersController {

    @Autowired
    R_headquartersService rhqs;

    @GetMapping("/showRhqAll")
    public List<R_headquarters> showRhqAll(){
        return rhqs.showRhqAll();
    }

}
