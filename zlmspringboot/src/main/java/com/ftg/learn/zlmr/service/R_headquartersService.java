package com.ftg.learn.zlmr.service;

import com.ftg.learn.zlmr.mapper.R_headquartersMapper;
import com.ftg.learn.zlmr.pojo.R_headquarters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/11 16:45
 */
@Service
public class R_headquartersService {

    @Autowired
    R_headquartersMapper rhqm;

    public List<R_headquarters> showRhqAll(){
        return rhqm.showRhqAll();
    }

}
