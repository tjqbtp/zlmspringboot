package com.ftg.learn.zlmr.service;

import com.ftg.learn.zlmr.mapper.R_managementteamMapper;
import com.ftg.learn.zlmr.pojo.R_managementteam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/11 16:45
 */
@Service
public class R_managementteamService {

    @Autowired
    R_managementteamMapper rmtm;

    public List<R_managementteam> showRmtAll(){
        return rmtm.showRmtAll();
    }

}
