package com.ftg.learn.zlmr.pojo;

/**
 * @author TJQ
 * @data 2019/10/11 16:29
 * 管理团队表
 */
public class R_managementteam {

    private String r_mt_id; // id
    private String r_mt_num; // 编号
    private String r_mt_name; // 姓名
    private String r_mt_jointime; // 加入时间
    private String r_mt_introduce; // 介绍
    private String r_mt_position; // 职位
    private String r_mt_picture; // 图片

    public String getR_mt_id() {
        return r_mt_id;
    }

    public void setR_mt_id(String r_mt_id) {
        this.r_mt_id = r_mt_id;
    }

    public String getR_mt_num() {
        return r_mt_num;
    }

    public void setR_mt_num(String r_mt_num) {
        this.r_mt_num = r_mt_num;
    }

    public String getR_mt_name() {
        return r_mt_name;
    }

    public void setR_mt_name(String r_mt_name) {
        this.r_mt_name = r_mt_name;
    }

    public String getR_mt_jointime() {
        return r_mt_jointime;
    }

    public void setR_mt_jointime(String r_mt_jointime) {
        this.r_mt_jointime = r_mt_jointime;
    }

    public String getR_mt_introduce() {
        return r_mt_introduce;
    }

    public void setR_mt_introduce(String r_mt_introduce) {
        this.r_mt_introduce = r_mt_introduce;
    }

    public String getR_mt_position() {
        return r_mt_position;
    }

    public void setR_mt_position(String r_mt_position) {
        this.r_mt_position = r_mt_position;
    }

    public String getR_mt_picture() {
        return r_mt_picture;
    }

    public void setR_mt_picture(String r_mt_picture) {
        this.r_mt_picture = r_mt_picture;
    }
}
