package com.ftg.learn.zlmr.service;

import com.ftg.learn.zlmr.mapper.R_dynamicMapper;
import com.ftg.learn.zlmr.pojo.R_dynamic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/11 16:44
 */
@Service
public class R_dynamicService {

    @Autowired
    R_dynamicMapper rdm;

    public List<R_dynamic> showRdAll(String curr, String limit){
        return rdm.showRdAll((Integer.valueOf(curr)-1)*Integer.valueOf(limit),Integer.valueOf(limit));
    }
    public int showCount(){return rdm.showCount();}

}
