package com.ftg.learn.zlmr.mapper;

import com.ftg.learn.zlmr.pojo.R_dynamic;
import com.ftg.learn.zlmr.pojo.R_subsidiary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/11 16:42
 */
@Mapper
public interface R_subsidiaryMapper {

    @Select("select * from r_subsidiary limit #{arg0},#{arg1}")
    public List<R_subsidiary> showRsAll(int curr, int limit);

    @Select("select count(r_s_id) from r_subsidiary")
    public int showRsCount();

}
