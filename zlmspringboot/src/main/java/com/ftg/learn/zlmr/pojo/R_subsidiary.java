package com.ftg.learn.zlmr.pojo;

/**
 * @author TJQ
 * @data 2019/10/11 16:29
 * 前台子公司表
 */
public class R_subsidiary {

    private String r_s_id; // id
    private String r_s_num; // 子公司编号
    private String r_s_name; // 子公司名
    private String r_s_desc; // 子公司描述（富文本）
    private String r_s_phone; // 子公司电话
    private String r_s_email; // 子公司邮箱
    private String r_s_picture; // 子公司图片
    private String r_s_url; // 子公司链接
    private String r_s_hqid; // 总公司id

    public String getR_s_id() {
        return r_s_id;
    }

    public void setR_s_id(String r_s_id) {
        this.r_s_id = r_s_id;
    }

    public String getR_s_num() {
        return r_s_num;
    }

    public void setR_s_num(String r_s_num) {
        this.r_s_num = r_s_num;
    }

    public String getR_s_name() {
        return r_s_name;
    }

    public void setR_s_name(String r_s_name) {
        this.r_s_name = r_s_name;
    }

    public String getR_s_desc() {
        return r_s_desc;
    }

    public void setR_s_desc(String r_s_desc) {
        this.r_s_desc = r_s_desc;
    }

    public String getR_s_phone() {
        return r_s_phone;
    }

    public void setR_s_phone(String r_s_phone) {
        this.r_s_phone = r_s_phone;
    }

    public String getR_s_email() {
        return r_s_email;
    }

    public void setR_s_email(String r_s_email) {
        this.r_s_email = r_s_email;
    }

    public String getR_s_picture() {
        return r_s_picture;
    }

    public void setR_s_picture(String r_s_picture) {
        this.r_s_picture = r_s_picture;
    }

    public String getR_s_url() {
        return r_s_url;
    }

    public void setR_s_url(String r_s_url) {
        this.r_s_url = r_s_url;
    }

    public String getR_s_hqid() {
        return r_s_hqid;
    }

    public void setR_s_hqid(String r_s_hqid) {
        this.r_s_hqid = r_s_hqid;
    }
}
