package com.ftg.learn.zlmr.controller;

import com.ftg.learn.zlmr.pojo.R_feedinfor;
import com.ftg.learn.zlmr.service.R_feedinforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class R_feedinforController {
    @Autowired
    R_feedinforService fs;
    @PostMapping("/addfeedinfor")
    public int addFeedInfor(R_feedinfor fi){

        return fs.addFeedInfor(fi);
    }

}
