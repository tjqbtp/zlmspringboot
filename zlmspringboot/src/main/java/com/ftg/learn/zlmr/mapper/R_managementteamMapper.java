package com.ftg.learn.zlmr.mapper;

import com.ftg.learn.zlmr.pojo.R_managementteam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/11 16:42
 */
@Mapper
public interface R_managementteamMapper {

    @Select("select * from r_managementteam order by r_mt_id asc")
    public List<R_managementteam> showRmtAll();

}
