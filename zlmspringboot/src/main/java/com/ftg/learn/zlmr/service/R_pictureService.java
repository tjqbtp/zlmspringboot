package com.ftg.learn.zlmr.service;

import com.ftg.learn.zlmr.mapper.R_pictureMapper;
import com.ftg.learn.zlmr.pojo.R_picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/12 9:03
 */
@Service
public class R_pictureService {

    @Autowired
    R_pictureMapper rpm;

    public List<R_picture> showRpAll(){
        return rpm.showRpAll();
    }

}
