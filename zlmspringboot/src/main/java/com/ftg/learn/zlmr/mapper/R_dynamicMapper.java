package com.ftg.learn.zlmr.mapper;

import com.ftg.learn.zlmr.pojo.R_dynamic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/11 16:40
 */
@Mapper
public interface R_dynamicMapper {

    @Select("select * from r_dynamic limit #{arg0},#{arg1}")
    public List<R_dynamic> showRdAll(int curr, int limit);
    @Select("select count(r_dynamic_id) from r_dynamic")
    public int showCount();

}
