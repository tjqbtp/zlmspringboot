package com.ftg.learn.zlmr.controller;

import com.ftg.learn.zlmr.pojo.R_managementteam;
import com.ftg.learn.zlmr.service.R_managementteamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/11 16:47
 */
@RestController
@CrossOrigin
@RequestMapping("/rmt")
public class R_managementteamController {

    @Autowired
    R_managementteamService rmts;

    @GetMapping("/showRmtAll")
    public List<R_managementteam> showRmtAll(){
        return rmts.showRmtAll();
    }

}
