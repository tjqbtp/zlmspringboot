package com.ftg.learn.zlmr.controller;

import com.ftg.learn.zlmr.service.R_dynamicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @author TJQ
 * @data 2019/10/11 16:47
 */
@RestController
@CrossOrigin
@RequestMapping("/rd")
public class R_dynamicController {

    @Autowired
    R_dynamicService rds;

    @GetMapping("/showRdAll")
    public Map<String,Object> showRdAll(String curr,String limit){
        Map<String,Object> m = new HashMap<>();
        m.put("lr",rds.showRdAll(curr,limit));
        m.put("i",rds.showCount());
        return m;
    }

}
