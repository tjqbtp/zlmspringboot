package com.ftg.learn.zlmr.controller;

import com.ftg.learn.zlmr.pojo.R_picture;
import com.ftg.learn.zlmr.service.R_pictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/12 9:05
 */
@RestController
@CrossOrigin
@RequestMapping("/rp")
public class R_pictureController {

    @Autowired
    R_pictureService rps;

    @GetMapping("/showRpAll")
    public List<R_picture> showRpAll(){
        return rps.showRpAll();
    }

}
