package com.ftg.learn.zlmr.controller;

import com.ftg.learn.zlmr.pojo.R_subsidiary;
import com.ftg.learn.zlmr.service.R_subsidiaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author TJQ
 * @data 2019/10/11 16:48
 * 前台子公司表ontroller层
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/rs")
public class R_subsidiaryController {

    @Autowired
    R_subsidiaryService rss;

    @GetMapping("/showRsAll")
    public Map<String,Object> showRsAll(String curr, String limit){
        Map<String,Object> m = new HashMap<>();
        m.put("lr",rss.showRsAll(curr,limit));
        m.put("i",rss.showRsCount());
        return m;
    }

}
