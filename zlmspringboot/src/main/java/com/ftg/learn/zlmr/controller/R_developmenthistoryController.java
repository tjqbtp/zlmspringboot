package com.ftg.learn.zlmr.controller;

import com.ftg.learn.zlmr.pojo.R_developmenthistory;
import com.ftg.learn.zlmr.service.R_developmenthistoryService;
import com.ftg.learn.zlmr.service.R_managementteamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author TJQ
 * @data 2019/10/11 16:46
 */
@RestController
@CrossOrigin
@RequestMapping("/rdh")
public class R_developmenthistoryController {

    @Autowired
    R_developmenthistoryService rdhs;
    @Autowired
    R_managementteamService rmts;

    @GetMapping("/showRdhAll")
    public List<R_developmenthistory> showRdhAll(){
        return rdhs.showRdhAll();
    }

    @GetMapping("/showAbout")
    public Map<String, Object> showAbout(){
        Map<String, Object> map = new HashMap<>();
        map.put("rdh", rdhs.showRdhAll());
        map.put("rmt", rmts.showRmtAll());
        return map;
    }

}
