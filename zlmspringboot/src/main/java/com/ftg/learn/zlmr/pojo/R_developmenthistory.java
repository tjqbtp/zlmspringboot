package com.ftg.learn.zlmr.pojo;

/**
 * @author TJQ
 * @data 2019/10/11 16:27
 * 发展历程表
 */
public class R_developmenthistory {

    private String r_dh_id; // id
    private String r_dh_num; // 编号
    private String r_dh_time; // 时间
    private String r_dh_title; // 标题
    private String r_dh_remarks; // 备注

    public String getR_dh_id() {
        return r_dh_id;
    }

    public void setR_dh_id(String r_dh_id) {
        this.r_dh_id = r_dh_id;
    }

    public String getR_dh_num() {
        return r_dh_num;
    }

    public void setR_dh_num(String r_dh_num) {
        this.r_dh_num = r_dh_num;
    }

    public String getR_dh_time() {
        return r_dh_time;
    }

    public void setR_dh_time(String r_dh_time) {
        this.r_dh_time = r_dh_time;
    }

    public String getR_dh_title() {
        return r_dh_title;
    }

    public void setR_dh_title(String r_dh_title) {
        this.r_dh_title = r_dh_title;
    }

    public String getR_dh_remarks() {
        return r_dh_remarks;
    }

    public void setR_dh_remarks(String r_dh_remarks) {
        this.r_dh_remarks = r_dh_remarks;
    }
}
