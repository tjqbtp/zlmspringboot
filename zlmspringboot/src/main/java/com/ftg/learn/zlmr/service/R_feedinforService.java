package com.ftg.learn.zlmr.service;

import com.ftg.learn.zlmr.mapper.R_feedinforMapper;
import com.ftg.learn.zlmr.pojo.R_feedinfor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class R_feedinforService {
    @Autowired
    R_feedinforMapper fim;

    public int addFeedInfor(R_feedinfor fi){
        return fim.addFeedInfor(fi);
    }

}
