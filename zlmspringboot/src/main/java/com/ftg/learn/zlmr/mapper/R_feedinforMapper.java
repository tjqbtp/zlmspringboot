package com.ftg.learn.zlmr.mapper;

import com.ftg.learn.zlmr.pojo.R_feedinfor;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.web.bind.annotation.RequestParam;

@Mapper
public interface R_feedinforMapper {
    @Insert("INSERT INTO `zlmfoodchain`.`r_feedbackinfor` (`phone`, `email`, `title`, `text`) VALUES (#{phone},#{email},#{title},#{text})")
    public int addFeedInfor(R_feedinfor fi);
}
