package com.ftg.learn.zlmr.pojo;

/**
 * @author TJQ
 * @data 2019/10/11 16:28
 * 总部表
 */
public class R_headquarters {

    private String r_hq_id; // id
    private String r_hq_num; // 总部编号
    private String r_hq_name; // 总部名
    private String r_hq_bi; // 总部简介
    private String r_hq_introduce; // 介绍
    private String r_hq_phone; // 总部电话
    private String r_hq_email; // 总部邮箱
    private String r_hq_address; // 总部地址

    public String getR_hq_id() {
        return r_hq_id;
    }

    public void setR_hq_id(String r_hq_id) {
        this.r_hq_id = r_hq_id;
    }

    public String getR_hq_num() {
        return r_hq_num;
    }

    public void setR_hq_num(String r_hq_num) {
        this.r_hq_num = r_hq_num;
    }

    public String getR_hq_name() {
        return r_hq_name;
    }

    public void setR_hq_name(String r_hq_name) {
        this.r_hq_name = r_hq_name;
    }

    public String getR_hq_bi() {
        return r_hq_bi;
    }

    public void setR_hq_bi(String r_hq_bi) {
        this.r_hq_bi = r_hq_bi;
    }

    public String getR_hq_introduce() {
        return r_hq_introduce;
    }

    public void setR_hq_introduce(String r_hq_introduce) {
        this.r_hq_introduce = r_hq_introduce;
    }

    public String getR_hq_phone() {
        return r_hq_phone;
    }

    public void setR_hq_phone(String r_hq_phone) {
        this.r_hq_phone = r_hq_phone;
    }

    public String getR_hq_email() {
        return r_hq_email;
    }

    public void setR_hq_email(String r_hq_email) {
        this.r_hq_email = r_hq_email;
    }

    public String getR_hq_address() {
        return r_hq_address;
    }

    public void setR_hq_address(String r_hq_address) {
        this.r_hq_address = r_hq_address;
    }
}
