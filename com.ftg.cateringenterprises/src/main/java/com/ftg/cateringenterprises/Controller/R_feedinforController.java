package com.ftg.cateringenterprises.Controller;


import com.ftg.cateringenterprises.Pojo.R_feedinfor;
import com.ftg.cateringenterprises.Service.R_feedinforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/R_feedinfor")
public class R_feedinforController {
    @Autowired
    R_feedinforService fs;

    @GetMapping("/showR_feedinfor")
    public Map<String, Object> showall(int page, int limit, String id) {
        Map<String, Object> map = new HashMap<>();
        System.out.println(page + "||" + limit + "||" + id);
        map.put("code", 0);
        map.put("msg", ".....");
        map.put("data", fs.showAll(id, page, limit));
        map.put("count", fs.getCount(id));
        return map;
    }

    @DeleteMapping("/delR_feedinfor/{ids}")
    public boolean delPicture(@PathVariable("ids") String[] ids) {
        System.out.println("--->");
        return fs.delR_feedinfor(ids);
    }

    @PostMapping("/addfeedinfor")
    public int addFeedInfor(R_feedinfor fi) {
        System.out.println("*****");
        return fs.addFeedInfor(fi);
    }

}
