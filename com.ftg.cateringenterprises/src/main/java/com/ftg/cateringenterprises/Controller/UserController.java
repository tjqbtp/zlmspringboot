package com.ftg.cateringenterprises.Controller;

import com.ftg.cateringenterprises.Pojo.User;
import com.ftg.cateringenterprises.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author TJQ
 * @data 2019/10/14 20:48
 */
@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService us;

    @GetMapping("/showUser")
    public Map<String,Object> showall(int page, int limit, String id){
        Map<String,Object> map = new HashMap<>();
        System.out.println(page+"||"+limit+"||"+id);
        map.put("code", 0);
        map.put("msg", ".....");
        map.put("data", us.showAll(id,page,limit));
        map.put("count", us.getCount(id));
        return map;//ת
    }

    @PostMapping("/addUser")
    public int addDevelopmenthistory(User u) {
        return us.addUser(u);
    }

    @DeleteMapping("/delUser/{ids}")
    public boolean delUser(@PathVariable("ids") String[] ids) {
        System.out.println("--->");
        return us.delUser(ids);
    }


    @PostMapping("/updateUser")
    public int updateUser(User u) {
        System.out.println("--->");
        return us.updateUser(u);
    }


    @PostMapping("/findUserOne")
    public User findUserOne(User u, HttpServletRequest request){
        List<User> a = us.findUserOne(u);
        System.out.println("--->");

        if(a != null&&a.size()>0){
            HttpSession session=request.getSession() ;
            session.setAttribute("username","ok");
           return a.get(0);
        }
        return null;
    }

}
