package com.ftg.cateringenterprises.Controller;

import com.ftg.cateringenterprises.Pojo.Dynamic;
import com.ftg.cateringenterprises.Pojo.Headquarters;
import com.ftg.cateringenterprises.Pojo.Subsidiary;
import com.ftg.cateringenterprises.Service.DynamicService;
import com.ftg.cateringenterprises.Service.SubsidiaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Max;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 数据处理模块
 * @author ylShi
 */
@RestController
@RequestMapping("/Subsidiary")
public class SubsidiaryController {
    @Autowired
    SubsidiaryService ds;

    @GetMapping("/showSubsidiary")
    public Map<String,Object> showall( int page, int limit, String id){
        Map<String,Object> map = new HashMap<>();
        System.out.println(page+"||"+limit+"||"+id);
        map.put("code", 0);
        map.put("msg", ".....");
        map.put("data", ds.showAll(id,page,limit));
        map.put("count", ds.getCount(id));
        return map;
    }

    @GetMapping("/showHq")
       public List<Headquarters> show(){
        return ds.showhq();
    }

    @PostMapping("/addSubsidiary")
    public int addSubsidiary(Subsidiary sd) {
        System.out.println(sd.getR_s_id());
        return ds.addSubsidiary(sd);
    }

    @DeleteMapping("/delSubsidiary/{ids}")
    public boolean delSubsidiary(@PathVariable("ids") String[] ids) {
        System.out.println("--->");
        return ds.delSubsidiary(ids);
    }

    @PutMapping("/findone/{id}")
    public Subsidiary findOne(@PathVariable("id") String id) {
        return ds.findOne(id);
    }

    @PostMapping("/updateSubsidiary")
    public int updateSubsidiary(Subsidiary sd) {
        System.out.println("--->");
        return ds.updateSubsidiary(sd);
    }

    @PostMapping("/SubprojectPictureUpload")
    @ResponseBody
    public Map<String, Object> addPhoto(MultipartFile file) {
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> photos = new HashMap<>();
        if (file.isEmpty()) {
            System.out.println("文件为空");
        }
        String fileName = file.getOriginalFilename();
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        String filePath = "F:\\zlmfoodchain\\images\\rs\\"; // 上传后的路径
//        \src\main\resources\public\img\  System.getProperty("user.dir")+
        fileName = UUID.randomUUID() + suffixName;
        File dest = new File(filePath + fileName);
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        photos.put("src","http://localhost:10025/rs/" + fileName);
        map.put("data",photos);
        return map;
    }
}
