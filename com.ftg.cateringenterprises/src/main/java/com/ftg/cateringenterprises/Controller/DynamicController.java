package com.ftg.cateringenterprises.Controller;

import com.ftg.cateringenterprises.Pojo.Dynamic;
import com.ftg.cateringenterprises.Service.DynamicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 数据处理模块
 * @author ylShi
 */
@RestController
@RequestMapping("/Dynamic")
class DynamicController {
    @Autowired
    DynamicService ds;

    @GetMapping("/showDynamic")
    public Map<String,Object> showall( int page, int limit, String id){
        Map<String,Object> map = new HashMap<>();
        System.out.println(page+"||"+limit+"||"+id);
        map.put("code", 0);
        map.put("msg", ".....");
        map.put("data", ds.showAll(id,page,limit));
        map.put("count", ds.getCount(id));
        return map;
    }

    @PostMapping("/addDynamic")
    public int addDynamic(Dynamic dc) {
        System.out.println(dc.getR_dynamic_id());
        return ds.addDynamic(dc);
    }

    @DeleteMapping("/delDynamic/{ids}")
    public boolean delDynamic(@PathVariable("ids") String[] ids) {
        System.out.println("--->");
        return ds.delDynamic(ids);
    }

    @PutMapping("/findone/{id}")
    public Dynamic findOne(@PathVariable("id") String id) {
        return ds.findOne(id);
    }

    @PostMapping("/updateDynamic")
    public int updateDynamic(Dynamic dc) {
        System.out.println("--->");
        return ds.updateDynamic(dc);
    }

    @PostMapping("/DJprojectPictureUpload")
    @ResponseBody
    public Map<String, Object> addPhoto(MultipartFile file) {
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> photos = new HashMap<>();
        if (file.isEmpty()) {
            System.out.println("文件为空");
        }
        String fileName = file.getOriginalFilename();
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        String filePath = "F:\\zlmfoodchain\\images\\rd\\"; // 上传后的路径
        fileName = UUID.randomUUID() + suffixName;
        File dest = new File(filePath + fileName);
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        photos.put("src","http://localhost:10025/rd/" + fileName);
        map.put("data",photos);
        return map;

    }

}
