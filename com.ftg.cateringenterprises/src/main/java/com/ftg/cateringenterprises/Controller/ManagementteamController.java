package com.ftg.cateringenterprises.Controller;

import com.ftg.cateringenterprises.Pojo.Developmenthistory;
import com.ftg.cateringenterprises.Pojo.Managementteam;
import com.ftg.cateringenterprises.Service.DevelopmenthistoryService;
import com.ftg.cateringenterprises.Service.ManagementteamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 数据处理模块
 * @author ylShi
 */
@RestController
@RequestMapping("/Managementteam")
public class ManagementteamController {
    @Autowired
    ManagementteamService ds;

    @GetMapping("/showManagementteam")
    public Map<String,Object> showall( int page, int limit, String id){
        Map<String,Object> map = new HashMap<>();
        System.out.println(page+"||"+limit+"||"+id);
        map.put("code", 0);
        map.put("msg", ".....");
        map.put("data", ds.showAll(id,page,limit));
        map.put("count", ds.getCount(id));
        return map;//ת
    }

    @PostMapping("/addManagementteam")
    public int addManagementteam(Managementteam mt) {
        System.out.println(mt.getR_mt_id());
        return ds.addManagementteam(mt);
    }

    @DeleteMapping("/delManagementteam/{ids}")
    public boolean delManagementteam(@PathVariable("ids") String[] ids) {
        System.out.println("--->");
        return ds.delManagementteam(ids);
    }

    @PutMapping("/findone/{id}")
    public Managementteam findOne(@PathVariable("id") String id) {
        return ds.findOne(id);
    }

    @PostMapping("/updateManagementteam")
    public int updateManagementteam(Managementteam mt) {
        System.out.println("--->");
        return ds.updateManagementteam(mt);
    }

    @PostMapping("/McprojectPictureUpload")
    @ResponseBody
    public Map<String, Object> addPhoto(MultipartFile file) {
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> photos = new HashMap<>();
        if (file.isEmpty()) {
            System.out.println("文件为空");
        }
        String fileName = file.getOriginalFilename();
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        String filePath = "F:\\zlmfoodchain\\images\\rmt\\"; // 上传后的路径
        fileName = UUID.randomUUID() + suffixName;
        File dest = new File(filePath + fileName);
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        photos.put("src","http://localhost:10025/rmt/" + fileName);
        map.put("data",photos);
        return map;

    }

}
