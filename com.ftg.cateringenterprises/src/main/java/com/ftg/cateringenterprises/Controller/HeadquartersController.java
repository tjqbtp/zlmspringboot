package com.ftg.cateringenterprises.Controller;


import com.ftg.cateringenterprises.Pojo.Headquarters;
import com.ftg.cateringenterprises.Service.HeadquartersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 数据处理模块
 * @author ylShi
 */
@RestController
@RequestMapping("/Headquarters")
public class HeadquartersController {
    @Autowired
    HeadquartersService ds;

    @GetMapping("/showHeadquarters")
    public Map<String,Object> showall( int page, int limit, String id){
        Map<String,Object> map = new HashMap<>();
        System.out.println(page+"||"+limit+"||"+id);
        map.put("code", 0);
        map.put("msg", ".....");
        map.put("data", ds.showAll(id,page,limit));
        map.put("count", ds.getCount(id));
        return map;
    }

    @PostMapping("/addHeadquarters")
    public int addHeadquarters(Headquarters ht) {
        System.out.println(ht.getR_hq_id());
        return ds.addHeadquarters(ht);
    }

    @DeleteMapping("/delHeadquarters/{ids}")
    public boolean delHeadquarters(@PathVariable("ids") String[] ids) {
        System.out.println("--->");
        return ds.delHeadquarters(ids);
    }

    @PutMapping("/findone/{id}")
    public Headquarters findOne(@PathVariable("id") String id) {
        return ds.findOne(id);
    }

    @PostMapping("/updateHeadquarters")
    public int updateHeadquarters(Headquarters ht) {
        System.out.println("--->");
        return ds.updateHeadquarters(ht);
    }

}
