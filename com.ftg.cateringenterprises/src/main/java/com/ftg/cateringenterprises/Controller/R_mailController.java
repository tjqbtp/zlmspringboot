package com.ftg.cateringenterprises.Controller;



import com.ftg.cateringenterprises.Service.R_mailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author mcb
 * 2018年5月4日 下午3:52:30
 *
 */
@RestController
public class R_mailController {
    @Autowired
    @Qualifier(value = "sendbyemail")
    private R_mailService service;

    @PostMapping("/send")
    public String send(String email){
        System.out.println(email);
        String sender="zwdjcy@163.com";   //这个是发送人的邮箱
        String receiver=email;  //这个是接受人的邮箱
        String title="大菜敏信息";    //标题
        String text="您好，您提交的信息我们已经收到。我们将于24小时内通过邮件给您答复。";//内容

        String result=service.send(sender, receiver, title, text);
        return result;
    }

}
