package com.ftg.cateringenterprises.Controller;

import com.ftg.cateringenterprises.Pojo.Developmenthistory;
import com.ftg.cateringenterprises.Service.DevelopmenthistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 数据处理模块
 * @author ylShi
 */
@RestController
@CrossOrigin
@RequestMapping("/Developmenthistory")
class DevelopmenthistoryController {
    @Autowired
    DevelopmenthistoryService ds;

    @GetMapping("/showDevelopmenthistory")
    public Map<String,Object> showall( int page, int limit, String id){
        Map<String,Object> map = new HashMap<>();
        System.out.println(page+"||"+limit+"||"+id);
        map.put("code", 0);
        map.put("msg", ".....");
        map.put("data", ds.showAll(id,page,limit));
        map.put("count", ds.getCount(id));
        return map;//ת
    }

    @PostMapping("/addDevelopmenthistory")
    public int addDevelopmenthistory(Developmenthistory dptt) {
        System.out.println(dptt.getR_dh_id());
        return ds.addDevelopmenthistory(dptt);
    }

    @DeleteMapping("/delDevelopmenthistory/{ids}")
    public boolean delDevelopmenthistory(@PathVariable("ids") String[] ids) {
        System.out.println("--->");
        return ds.delDevelopmenthistory(ids);
    }

    @PutMapping("/findone/{id}")
    public Developmenthistory findOne(@PathVariable("id") String id) {
        return ds.findOne(id);
    }

    @PostMapping("/updateDevelopmenthistory")
    public int updateDevelopmenthistory(Developmenthistory dptt) {
        System.out.println("--->");
        return ds.updateDevelopmenthistory(dptt);
    }

    @PostMapping("/projectPictureUpload")
    @ResponseBody
    public Map<String, Object> addPhoto(MultipartFile file) {
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> photos = new HashMap<>();
        if (file.isEmpty()) {
            System.out.println("文件为空");
        }
        String fileName = file.getOriginalFilename();
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        String filePath = "F:\\zlmfoodchain\\images\\rp\\"; // 上传后的路径
//        String filePath = System.getProperty("user.dir")+"\\src\\main\\resources\\public\\img\\"; // 上传后的路径
        fileName = UUID.randomUUID() + suffixName;
        File dest = new File(filePath + fileName);
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        photos.put("src","http://localhost:10025/rp/" + fileName);
//        photos.put("src","img/" + fileName);
        map.put("data",photos);
        return map;

    }

}
