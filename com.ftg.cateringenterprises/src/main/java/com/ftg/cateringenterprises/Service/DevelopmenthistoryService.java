package com.ftg.cateringenterprises.Service;

import com.ftg.cateringenterprises.Mapper.DevelopmenthistoryMapper;
import com.ftg.cateringenterprises.Pojo.Developmenthistory;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * 业务处理层
 */
@Service
public class DevelopmenthistoryService {

    @Autowired
    DevelopmenthistoryMapper dd;

    public List<Developmenthistory> showAll(String id, int page, int limit){
        return dd.showAll(id,(page-1)*limit,limit);
    }

    public int addDevelopmenthistory(Developmenthistory dptt) {
        return dd.addDevelopmenthistory(dptt);
    }

    public boolean delDevelopmenthistory(String[] ids) {
        try {
            for(String id : ids) {
                dd.delDevelopmenthistory(id);
            }
            return true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Developmenthistory findOne(String id) {
        return dd.findOne(id);
    }

    public int updateDevelopmenthistory(Developmenthistory dptt) {
        return dd.updateDevelopmenthistory(dptt);
    }

    public int getCount(String id){
        return dd.getCount(id).size();
    }
}
