package com.ftg.cateringenterprises.Service;


import com.ftg.cateringenterprises.Mapper.ManagementteamMapper;
import com.ftg.cateringenterprises.Pojo.Managementteam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 业务处理层
 */
@Service
public class ManagementteamService {

    @Autowired
    ManagementteamMapper dd;

    public List<Managementteam> showAll(String id, int page, int limit){
        return dd.showAll(id,(page-1)*limit,limit);
    }

    public int addManagementteam(Managementteam mt) {
        return dd.addManagementteam(mt);
    }

    public boolean delManagementteam(String[] ids) {
        try {
            for(String id : ids) {
                dd.delManagementteam(id);
            }
            return true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Managementteam findOne(String id) {
        return dd.findOne(id);
    }

    public int updateManagementteam(Managementteam mt) {
        return dd.updateManagementteam(mt);
    }

    public int getCount(String id){
        return dd.getCount(id).size();
    }
}
