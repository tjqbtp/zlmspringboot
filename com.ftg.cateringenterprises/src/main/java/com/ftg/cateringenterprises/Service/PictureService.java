package com.ftg.cateringenterprises.Service;

import com.ftg.cateringenterprises.Mapper.DevelopmenthistoryMapper;
import com.ftg.cateringenterprises.Mapper.PictureMapper;
import com.ftg.cateringenterprises.Pojo.Developmenthistory;
import com.ftg.cateringenterprises.Pojo.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 业务处理层
 */
@Service
public class PictureService {

    @Autowired
    PictureMapper dd;

    public List<Picture> showAll(String id, int page, int limit){
        return dd.showAll(id,(page-1)*limit,limit);
    }

    public int addPicture(Picture pt) {
        return dd.addPicture(pt);
    }

    public boolean delPicture(String[] ids) {
        try {
            for(String id : ids) {
                dd.delPicture(id);
            }
            return true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Picture findOne(String id) {
        return dd.findOne(id);
    }

    public int updatePicture(Picture pt) {
        return dd.updatePicture(pt);
    }

    public int getCount(String id){
        return dd.getCount(id).size();
    }


    public List<Picture> selone(String id){
        return dd.oneSinger(id);
    }

}
