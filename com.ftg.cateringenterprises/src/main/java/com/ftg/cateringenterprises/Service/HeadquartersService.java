package com.ftg.cateringenterprises.Service;


import com.ftg.cateringenterprises.Mapper.HeadquartersMapper;
import com.ftg.cateringenterprises.Pojo.Headquarters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 业务处理层
 */
@Service
public class HeadquartersService {

    @Autowired
    HeadquartersMapper dd;

    public List<Headquarters> showAll(String id, int page, int limit){
        return dd.showAll(id,(page-1)*limit,limit);
    }

    public int addHeadquarters(Headquarters ht) {
        return dd.addHeadquarters(ht);
    }

    public boolean delHeadquarters(String[] ids) {
        try {
            for(String id : ids) {
                dd.delHeadquarters(id);
            }
            return true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Headquarters findOne(String id) {
        return dd.findOne(id);
    }

    public int updateHeadquarters(Headquarters ht) {
        return dd.updateHeadquarters(ht);
    }

    public int getCount(String id){
        return dd.getCount(id).size();
    }
}
