package com.ftg.cateringenterprises.Service;

import com.ftg.cateringenterprises.Mapper.DevelopmenthistoryMapper;
import com.ftg.cateringenterprises.Mapper.DynamicMapper;
import com.ftg.cateringenterprises.Pojo.Developmenthistory;
import com.ftg.cateringenterprises.Pojo.Dynamic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 业务处理层
 */
@Service
public class DynamicService {

    @Autowired
    DynamicMapper dd;

    public List<Dynamic> showAll(String id, int page, int limit){
        return dd.showAll(id,(page-1)*limit,limit);
    }

    public int addDynamic(Dynamic dc) {
        return dd.addDynamic(dc);
    }

    public boolean delDynamic(String[] ids) {
        try {
            for(String id : ids) {
                dd.delDynamic(id);
            }
            return true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Dynamic findOne(String id) {
        return dd.findOne(id);
    }

    public int updateDynamic(Dynamic dc) {
        return dd.updateDynamic(dc);
    }

    public int getCount(String id){
        return dd.getCount(id).size();
    }
}
