package com.ftg.cateringenterprises.Service;

import com.ftg.cateringenterprises.Mapper.UserMapper;
import com.ftg.cateringenterprises.Pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author TJQ
 * @data 2019/10/14 20:42
 */
@Service
public class UserService {

    @Autowired
    UserMapper um ;

    public List<User> showAll(String id, int page, int limit){
        return um.showAll(id,(page-1)*limit,limit);
    }

    public int addUser(User u){
        return um.addUser(u);
    }

    public boolean delUser(String[] ids) {
        try {
            for(String id : ids) {
                um.delUser(id);
            }
            return true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getCount(String id){
        return um.getCount(id).size();
    }

    public int updateUser(User u) {
        return um.updateUser(u);
    }

    public List<User> findUserOne(User u){

        return um.findUserOne(u);
    }

}
