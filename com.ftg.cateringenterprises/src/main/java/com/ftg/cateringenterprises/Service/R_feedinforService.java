package com.ftg.cateringenterprises.Service;


import com.ftg.cateringenterprises.Mapper.R_feedinforMapper;
import com.ftg.cateringenterprises.Pojo.R_feedinfor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class R_feedinforService {

    @Autowired
    R_feedinforMapper fim;

    public List<R_feedinfor> showAll(String id, int page, int limit){
        return fim.showAll(id,(page-1)*limit,limit);
    }

    public int addFeedInfor(R_feedinfor fi){
        return fim.addFeedInfor(fi);
    }

    public boolean delR_feedinfor(String[] ids) {
        try {
            for(String id : ids) {
                fim.delR_feedinfor(id);
            }
            return true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getCount(String id){
        return fim.getCount(id).size();
    }

}
