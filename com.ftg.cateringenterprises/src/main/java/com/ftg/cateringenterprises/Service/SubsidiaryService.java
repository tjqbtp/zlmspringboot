package com.ftg.cateringenterprises.Service;

import com.ftg.cateringenterprises.Mapper.SubsidiaryMapper;
import com.ftg.cateringenterprises.Pojo.Headquarters;
import com.ftg.cateringenterprises.Pojo.Subsidiary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 业务处理层
 */
@Service
public class SubsidiaryService {

    @Autowired
    SubsidiaryMapper dd;

    public List<Map<String,Object>> showAll(String id, int page, int limit){
        return dd.showAll(id,(page-1)*limit,limit);
    }

    public int addSubsidiary(Subsidiary sd) {
        return dd.addSubsidiary(sd);
    }

    public boolean delSubsidiary(String[] ids) {
        try {
            for(String id : ids) {
                dd.delSubsidiary(id);
            }
            return true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Subsidiary findOne(String id) {
        return dd.findOne(id);
    }

    public int updateSubsidiary(Subsidiary sd) {
        return dd.updateSubsidiary(sd);
    }

    public int getCount(String id){
        return dd.getCount(id).size();
    }

    public List<Subsidiary> selone(String id){
        return dd.oneSinger(id);
    }

    public List<Headquarters> showhq(){ return  dd.showhq();}
}
