package com.ftg.cateringenterprises.Mapper;

import com.ftg.cateringenterprises.Pojo.Developmenthistory;
import org.apache.ibatis.annotations.*;
import java.util.List;

/**
 * 发展历程处理层
 */
@Mapper
public interface DevelopmenthistoryMapper {
    @SelectProvider(type=DevelopmenthistorySql.class,method="getShowAllSql")
    public List<Developmenthistory> showAll(String id, int begin, int end);

    @SelectProvider(type=DevelopmenthistorySql.class,method="getCountSql")
    public List<Developmenthistory> getCount(String id);

    @Insert("insert into r_developmenthistory(r_dh_num,r_dh_time,r_dh_title,r_dh_remarks) values(#{r_dh_num},#{r_dh_time},#{r_dh_title},#{r_dh_remarks})")
    public int addDevelopmenthistory(Developmenthistory dptt);

    @Delete("delete from r_developmenthistory where r_dh_id = #{id}")
    public void delDevelopmenthistory(String id);

    @Select("select * from r_developmenthistory where r_dh_id=#{id}")
    public Developmenthistory findOne(String id);

    @Update("update r_developmenthistory set r_dh_num=#{r_dh_num},r_dh_time=#{r_dh_time},r_dh_title=#{r_dh_title},r_dh_remarks=#{r_dh_remarks} where r_dh_id = #{r_dh_id}")
    public int updateDevelopmenthistory(Developmenthistory dptt);
}
