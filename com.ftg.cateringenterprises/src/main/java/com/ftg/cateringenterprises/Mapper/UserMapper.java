package com.ftg.cateringenterprises.Mapper;

import com.ftg.cateringenterprises.Pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * @author TJQ
 * @data 2019/10/14 20:48
 */
@Mapper
public interface UserMapper {

    @SelectProvider(type=UserSql.class,method="getShowAllSql")
    public List<User> showAll(String id, int begin, int end);

    @SelectProvider(type=UserSql.class,method="getCountSql")
    public List<User> getCount(String id);

    @Insert("insert into b_user(b_user_num,b_user_name,b_user_password,b_user_email,b_user_createtime,b_user_remarks,b_user_state) values(#{b_user_num},#{b_user_name},#{b_user_password},#{b_user_email},#{b_user_createtime},#{b_user_remarks},#{b_user_state})")
    public int addUser(User u);

    @Delete("delete from b_user where b_user_id = #{id}")
    public void delUser(String id);

    @Update("update b_user set b_user_num=#{b_user_num},b_user_name=#{b_user_name},b_user_password=#{b_user_password},b_user_email=#{b_user_email},b_user_createtime=#{b_user_createtime},b_user_remarks=#{b_user_remarks},b_user_state=#{b_user_state} where b_user_id = #{b_user_id}")
    public int updateUser(User u);

    @Select("select * from b_user where b_user_name = #{b_user_name} and b_user_password = #{b_user_password}")
    public List<User> findUserOne(User u);

}
