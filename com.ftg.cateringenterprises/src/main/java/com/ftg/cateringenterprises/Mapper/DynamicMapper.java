package com.ftg.cateringenterprises.Mapper;


import com.ftg.cateringenterprises.Pojo.Dynamic;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 发展历程处理层
 */
@Mapper
public interface DynamicMapper {
    @SelectProvider(type=DynamicSql.class,method="getShowAllSql")
    public List<Dynamic> showAll(String id, int begin, int end);

    @SelectProvider(type=DynamicSql.class,method="getCountSql")
    public List<Dynamic> getCount(String id);

    @Insert("insert into r_dynamic(r_dynamic_num,r_dynamic_author,r_dynamic_url,r_dynamic_time,r_dynamic_intro,r_dynamic_pic,r_dynamic_remarks,r_dynamic_title,r_dynamic_state) values(#{r_dynamic_num},#{r_dynamic_author},#{r_dynamic_url},#{r_dynamic_time},#{r_dynamic_intro},#{r_dynamic_pic},#{r_dynamic_remarks},#{r_dynamic_title},#{r_dynamic_state})")
    public int addDynamic(Dynamic dc);

    @Delete("delete from r_dynamic where r_dynamic_id = #{id}")
    public void delDynamic(String id);

    @Select("select * from r_dynamic where r_dynamic_id=#{id}")
    public Dynamic findOne(String id);

    @Update("update r_dynamic set r_dynamic_num=#{r_dynamic_num},r_dynamic_author=#{r_dynamic_author},r_dynamic_url=#{r_dynamic_url},r_dynamic_time=#{r_dynamic_time},r_dynamic_intro=#{r_dynamic_intro},r_dynamic_pic=#{r_dynamic_pic},r_dynamic_remarks=#{r_dynamic_remarks},r_dynamic_title=#{r_dynamic_title},r_dynamic_state=#{r_dynamic_state} where r_dynamic_id = #{r_dynamic_id}")
    public int updateDynamic(Dynamic dc);
}
