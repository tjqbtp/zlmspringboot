package com.ftg.cateringenterprises.Mapper;


import com.ftg.cateringenterprises.Pojo.Picture;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 发展历程处理层
 */
@Mapper
public interface PictureMapper {
    @SelectProvider(type=PictureSql.class,method="getShowAllSql")
    public List<Picture> showAll(String id, int begin, int end);

    @SelectProvider(type=PictureSql.class,method="getCountSql")
    public List<Picture> getCount(String id);

    @Insert("insert into r_picture(r_p_name,r_p_url,r_p_remarks) values(#{r_p_name},#{r_p_url},#{r_p_remarks})")
    public int addPicture(Picture pt);

    @Delete("delete from r_picture where r_p_id = #{id}")
    public void delPicture(String id);

    @Select("select * from r_picture where r_p_id=#{id}")
    public Picture findOne(String id);

    @Update("update r_picture set r_p_name=#{r_p_name},r_p_url=#{r_p_url},r_p_remarks=#{r_p_remarks} where r_p_id = #{r_p_id}")
    public int updatePicture(Picture pt);


    @Select("select * from r_picture where r_p_id=#{id}")
    public List<Picture> oneSinger(String id);


}
