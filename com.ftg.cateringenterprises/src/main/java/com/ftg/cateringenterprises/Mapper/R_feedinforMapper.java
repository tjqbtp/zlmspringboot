package com.ftg.cateringenterprises.Mapper;


import com.ftg.cateringenterprises.Pojo.R_feedinfor;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface R_feedinforMapper {
    @Insert("INSERT INTO `zlmfoodchain`.`r_feedbackinfor` (`phone`, `email`, `title`, `text`) VALUES (#{phone},#{email},#{title},#{text})")
    public int addFeedInfor( R_feedinfor fi);

    @SelectProvider(type=R_feedinforSql.class,method="getShowAllSql")
    public List<R_feedinfor> showAll(String id, int begin, int end);

    @SelectProvider(type=R_feedinforSql.class,method="getCountSql")
    public List<R_feedinfor> getCount(String id);

    @Delete("delete from r_feedbackinfor where id = #{id}")
    public void delR_feedinfor(String id);



//    @Insert("insert into r_feedbackinfor(phone,email,title,text) values(#{phone},#{email},#{title},#{text})")
//    public int addFeedInfor(R_feedinfor fi);
}
