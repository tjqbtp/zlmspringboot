package com.ftg.cateringenterprises.Mapper;


import com.ftg.cateringenterprises.Pojo.Headquarters;
import com.ftg.cateringenterprises.Pojo.Subsidiary;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 发展历程处理层
 */
@Mapper
public interface SubsidiaryMapper {
    @SelectProvider(type=SubsidiarySql.class,method="getShowAllSql")
    public List<Map<String,Object>> showAll(String id, int begin, int end);

    @SelectProvider(type=SubsidiarySql.class,method="getCountSql")
    public List<Subsidiary> getCount(String id);

    @Select("select * from r_headquarters")
    public List<Headquarters> showhq();

    @Insert("insert into r_subsidiary(r_s_num,r_s_name,r_s_desc,r_s_phone,r_s_email,r_s_picture,r_s_url,r_s_hqid) values(#{r_s_num},#{r_s_name},#{r_s_desc},#{r_s_phone},#{r_s_email},#{r_s_picture},#{r_s_url},#{r_s_hqid})")
    public int addSubsidiary(Subsidiary sd);

    @Delete("delete from r_subsidiary where r_s_id = #{id}")
    public void delSubsidiary(String id);

    @Select("select * from r_subsidiary where r_s_id=#{id}")
    public Subsidiary findOne(String id);

    @Update("update r_subsidiary set r_s_num=#{r_s_num},r_s_name=#{r_s_name},r_s_desc=#{r_s_desc},r_s_phone=#{r_s_phone},r_s_email=#{r_s_email},r_s_picture=#{r_s_picture},r_s_url=#{r_s_url},r_s_hqid=#{r_s_hqid} where r_s_id = #{r_s_id}")
    public int updateSubsidiary(Subsidiary sd);

    @Select("select * from r_subsidiary where r_s_id=#{id}")
    public List<Subsidiary> oneSinger(String id);
}
