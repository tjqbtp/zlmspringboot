package com.ftg.cateringenterprises.Mapper;


import com.ftg.cateringenterprises.Pojo.Headquarters;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 公司总部处理层
 */
@Mapper
public interface HeadquartersMapper {
    @SelectProvider(type=HeadquartersSql.class,method="getShowAllSql")
    public List<Headquarters> showAll(String id, int begin, int end);

    @SelectProvider(type=HeadquartersSql.class,method="getCountSql")
    public List<Headquarters> getCount(String id);

    @Insert("insert into r_headquarters(r_hq_num,r_hq_name,r_hq_bi,r_hq_introduce,r_hq_phone,r_hq_email,r_hq_address) values(#{r_hq_num},#{r_hq_name},#{r_hq_bi},#{r_hq_introduce},#{r_hq_phone},#{r_hq_email},#{r_hq_address})")
    public int addHeadquarters(Headquarters ht);

    @Delete("delete from r_headquarters where r_hq_id = #{id}")
    public void delHeadquarters(String id);

    @Select("select * from r_headquarters where r_hq_id=#{id}")
    public Headquarters findOne(String id);

    @Update("update r_headquarters set r_hq_num=#{r_hq_num},r_hq_name=#{r_hq_name},r_hq_bi=#{r_hq_bi},r_hq_introduce=#{r_hq_introduce},r_hq_phone=#{r_hq_phone},r_hq_email=#{r_hq_email},r_hq_address=#{r_hq_address} where r_hq_id=#{r_hq_id}")
    public int updateHeadquarters(Headquarters ht);
}
