package com.ftg.cateringenterprises.Mapper;
import org.apache.ibatis.jdbc.SQL;

public class UserSql {
    public String getCountSql(String id) {

        return new SQL() {{
            SELECT("* ");
            FROM("b_user");
            if(null != id && !"".equals(id)) {
                WHERE("b_user_id like concat(#{id},'%') ");
            }
        }}.toString();
    }


    public String getShowAllSql( String id, int begin, int end) {

        return new SQL() {{
            SELECT("* ");
            FROM("b_user");
            if(null != id && !"".equals(id)) {
                WHERE("b_user_id like concat(#{arg0},'%') ");
            }

        }}.toString() + " limit #{arg1},#{arg2}";
    }
}
