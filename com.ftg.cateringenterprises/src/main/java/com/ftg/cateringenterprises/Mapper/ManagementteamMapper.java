package com.ftg.cateringenterprises.Mapper;


import com.ftg.cateringenterprises.Pojo.Managementteam;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 发展历程处理层
 */
@Mapper
public interface ManagementteamMapper {
    @SelectProvider(type=ManagementteamSql.class,method="getShowAllSql")
    public List<Managementteam> showAll(String id, int begin, int end);

    @SelectProvider(type=ManagementteamSql.class,method="getCountSql")
    public List<Managementteam> getCount(String id);

    @Insert("insert into r_managementteam(r_mt_num,r_mt_name,r_mt_jointime,r_mt_introduce,r_mt_position,r_mt_picture) values(#{r_mt_num},#{r_mt_name},#{r_mt_jointime},#{r_mt_introduce},#{r_mt_position},#{r_mt_picture})")
    public int addManagementteam(Managementteam mt);

    @Delete("delete from r_managementteam where r_mt_id = #{id}")
    public void delManagementteam(String id);

    @Select("select * from r_managementteam where r_mt_id=#{id}")
    public Managementteam findOne(String id);

    @Update("update r_managementteam set r_mt_num=#{r_mt_num},r_mt_name=#{r_mt_name},r_mt_jointime=#{r_mt_jointime},r_mt_introduce=#{r_mt_introduce},r_mt_position=#{r_mt_position},r_mt_picture=#{r_mt_picture} where r_mt_id = #{r_mt_id}")
    public int updateManagementteam(Managementteam mt);
}
