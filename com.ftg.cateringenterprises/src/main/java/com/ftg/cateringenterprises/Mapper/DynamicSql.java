package com.ftg.cateringenterprises.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class DynamicSql {
    public String getCountSql(String id) {

        return new SQL() {{
            SELECT("* ");
            FROM("r_dynamic");
            if(null != id && !"".equals(id)) {
                WHERE("r_dynamic_id like concat(#{id},'%') ");
            }
        }}.toString();
    }


    public String getShowAllSql( String id, int begin, int end) {

        return new SQL() {{
            SELECT("* ");
            FROM("r_dynamic");
            if(null != id && !"".equals(id)) {
                WHERE("r_dynamic_id like concat(#{arg0},'%') ");
            }

        }}.toString() + " limit #{arg1},#{arg2}";
    }
}
