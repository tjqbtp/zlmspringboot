package com.ftg.cateringenterprises.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class SubsidiarySql {
    public String getCountSql(String id) {

        return new SQL() {{
            SELECT("* ");
            FROM("r_subsidiary");
            if(null != id && !"".equals(id)) {
                WHERE("r_s_id like concat(#{id},'%') ");
            }
        }}.toString();
    }


    public String getShowAllSql( String id,  int begin,  int end) {

        return new SQL() {{
            SELECT("rh.r_hq_name,rs.r_s_id,rs.r_s_num,rs.r_s_name,rs.r_s_desc,rs.r_s_phone,rs.r_s_email,rs.r_s_picture,rs.r_s_url,rs.r_s_hqid ");
            FROM("r_subsidiary rs left join r_headquarters rh on rs.r_s_hqid=rh.r_hq_id");
            if(null != id && !"".equals(id)) {
                WHERE("rs.r_s_id like concat(#{arg0},'%') ");
            }

        }}.toString() + " limit #{arg1},#{arg2}";
    }
}
