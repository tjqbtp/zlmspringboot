package com.ftg.cateringenterprises.Pojo;

/**
 * 公司动态类库
 */
public class Dynamic {
    private int r_dynamic_id;
    private String r_dynamic_num;
    private String r_dynamic_title;
    private String r_dynamic_author;
    private String r_dynamic_time;
    private String r_dynamic_url;
    private String r_dynamic_remarks;
    private String r_dynamic_state;
    private String r_dynamic_intro;
    private String r_dynamic_pic;

    public String getR_dynamic_intro() {
        return r_dynamic_intro;
    }

    public void setR_dynamic_intro(String r_dynamic_intro) {
        this.r_dynamic_intro = r_dynamic_intro;
    }

    public String getR_dynamic_pic() {
        return r_dynamic_pic;
    }

    public void setR_dynamic_pic(String r_dynamic_pic) {
        this.r_dynamic_pic = r_dynamic_pic;
    }

    /**
     * id
     * @return
     */
    public int getR_dynamic_id() {
        return r_dynamic_id;
    }

    public void setR_dynamic_id(int r_dynamic_id) {
        this.r_dynamic_id = r_dynamic_id;
    }

    /**
     * 动态编号
     * @return
     */
    public String getR_dynamic_num() {
        return r_dynamic_num;
    }

    public void setR_dynamic_num(String r_dynamic_num) {
        this.r_dynamic_num = r_dynamic_num;
    }

    /**
     * 动态标题
     * @return
     */
    public String getR_dynamic_title() {
        return r_dynamic_title;
    }

    public void setR_dynamic_title(String r_dynamic_title) {
        this.r_dynamic_title = r_dynamic_title;
    }

    /**
     * 作者
     * @return
     */
    public String getR_dynamic_author() {
        return r_dynamic_author;
    }

    public void setR_dynamic_author(String r_dynamic_author) {
        this.r_dynamic_author = r_dynamic_author;
    }

    /**
     * 发布时间
     * @return
     */
    public String getR_dynamic_time() {
        return r_dynamic_time;
    }

    public void setR_dynamic_time(String r_dynamic_time) {
        this.r_dynamic_time = r_dynamic_time;
    }

    /**
     * 动态链接
     * @return
     */
    public String getR_dynamic_url() {
        return r_dynamic_url;
    }

    public void setR_dynamic_url(String r_dynamic_url) {
        this.r_dynamic_url = r_dynamic_url;
    }

    /**
     * 备注
     * @return
     */
    public String getR_dynamic_remarks() {
        return r_dynamic_remarks;
    }

    public void setR_dynamic_remarks(String r_dynamic_remarks) {
        this.r_dynamic_remarks = r_dynamic_remarks;
    }

    /**
     * 状态
     * @return
     */
    public String getR_dynamic_state() {
        return r_dynamic_state;
    }

    public void setR_dynamic_state(String r_dynamic_state) {
        this.r_dynamic_state = r_dynamic_state;
    }
}
