package com.ftg.cateringenterprises.Pojo;

/**
 * 轮播图片类库
 */
public class Picture {
    private int r_p_id;
    private String r_p_name;
    private String r_p_url;
    private String r_p_remarks;

    /**
     * 图片id
     * @return
     */
    public int getR_p_id() {
        return r_p_id;
    }

    public void setR_p_id(int r_p_id) {
        this.r_p_id = r_p_id;
    }

    /**
     * 图片名
     * @return
     */
    public String getR_p_name() {
        return r_p_name;
    }

    public void setR_p_name(String r_p_name) {
        this.r_p_name = r_p_name;
    }

    /**
     * 图片路径
     * @return
     */
    public String getR_p_url() {
        return r_p_url;
    }

    public void setR_p_url(String r_p_url) {
        this.r_p_url = r_p_url;
    }

    /**
     * 备注
     * @return
     */
    public String getR_p_remarks() {
        return r_p_remarks;
    }

    public void setR_p_remarks(String r_p_remarks) {
        this.r_p_remarks = r_p_remarks;
    }
}
