package com.ftg.cateringenterprises.Pojo;

/**
 * 管理团队类库
 */
public class Managementteam {
    private int r_mt_id;
    private String r_mt_num;
    private String r_mt_name;
    private String r_mt_jointime;
    private String r_mt_introduce;
    private String r_mt_position;
    private String r_mt_picture;


    public String getR_mt_picture() {
        return r_mt_picture;
    }

    public void setR_mt_picture(String r_mt_picture) {
        this.r_mt_picture = r_mt_picture;
    }

    /**
     * id
     * @return
     */
    public int getR_mt_id() {
        return r_mt_id;
    }

    public void setR_mt_id(int r_mt_id) {
        this.r_mt_id = r_mt_id;
    }

    /**
     * 编号
     * @return
     */
    public String getR_mt_num() {
        return r_mt_num;
    }

    public void setR_mt_num(String r_mt_num) {
        this.r_mt_num = r_mt_num;
    }

    /**
     * 姓名
     * @return
     */
    public String getR_mt_name() {
        return r_mt_name;
    }

    public void setR_mt_name(String r_mt_name) {
        this.r_mt_name = r_mt_name;
    }

    /**
     * 加入时间
     * @return
     */
    public String getR_mt_jointime() {
        return r_mt_jointime;
    }

    public void setR_mt_jointime(String r_mt_jointime) {
        this.r_mt_jointime = r_mt_jointime;
    }

    /**
     * 介绍
     * @return
     */
    public String getR_mt_introduce() {
        return r_mt_introduce;
    }

    public void setR_mt_introduce(String r_mt_introduce) {
        this.r_mt_introduce = r_mt_introduce;
    }

    /**
     * 职位
     * @return
     */
    public String getR_mt_position() {
        return r_mt_position;
    }

    public void setR_mt_position(String r_mt_position) {
        this.r_mt_position = r_mt_position;
    }
}
