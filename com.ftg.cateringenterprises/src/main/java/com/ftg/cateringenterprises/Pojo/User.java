package com.ftg.cateringenterprises.Pojo;

/**
 * @author ylShi
 * @data 2019/10/14 20:36
 */
public class User {

    private int b_user_id;
    private String b_user_num;
    private String b_user_name;
    private String b_user_password;
    private String b_user_email;
    private String b_user_createtime;
    private String b_user_remarks;
    private String b_user_state;

    public int getB_user_id() {
        return b_user_id;
    }

    public void setB_user_id(int b_user_id) {
        this.b_user_id = b_user_id;
    }

    public String getB_user_num() {
        return b_user_num;
    }

    public void setB_user_num(String b_user_num) {
        this.b_user_num = b_user_num;
    }

    public String getB_user_name() {
        return b_user_name;
    }

    public void setB_user_name(String b_user_name) {
        this.b_user_name = b_user_name;
    }

    public String getB_user_password() {
        return b_user_password;
    }

    public void setB_user_password(String b_user_password) {
        this.b_user_password = b_user_password;
    }

    public String getB_user_email() {
        return b_user_email;
    }

    public void setB_user_email(String b_user_email) {
        this.b_user_email = b_user_email;
    }

    public String getB_user_createtime() {
        return b_user_createtime;
    }

    public void setB_user_createtime(String b_user_createtime) {
        this.b_user_createtime = b_user_createtime;
    }

    public String getB_user_remarks() {
        return b_user_remarks;
    }

    public void setB_user_remarks(String b_user_remarks) {
        this.b_user_remarks = b_user_remarks;
    }

    public String getB_user_state() {
        return b_user_state;
    }

    public void setB_user_state(String b_user_state) {
        this.b_user_state = b_user_state;
    }
}
