package com.ftg.cateringenterprises.Pojo;

/**
 * 发展历程类库
 */
public class Developmenthistory {
    private int r_dh_id;
    private String r_dh_num;
    private String r_dh_time;
    private String r_dh_title;
    private String r_dh_remarks;


    /**
     * id
     * @return id
     */
    public int getR_dh_id() {
        return r_dh_id;
    }

    public void setR_dh_id(int r_dh_id) {
        this.r_dh_id = r_dh_id;
    }

    /**
     * 编号
     * @return r_dh_num
     */

    public String getR_dh_num() {
        return r_dh_num;
    }

    public void setR_dh_num(String r_dh_num) {
        this.r_dh_num = r_dh_num;
    }

    /**
     * 时间
     * @return r_dh_time
     */
    public String getR_dh_time() {
        return r_dh_time;
    }

    public void setR_dh_time(String r_dh_time) {
        this.r_dh_time = r_dh_time;
    }

    /**
     * 标题
     * @return r_dh_title
     */
    public String getR_dh_title() {
        return r_dh_title;
    }

    public void setR_dh_title(String r_dh_title) {
        this.r_dh_title = r_dh_title;
    }

    /**
     * 备注
     * @return r_dh_remarks
     */
    public String getR_dh_remarks() {
        return r_dh_remarks;
    }

    public void setR_dh_remarks(String r_dh_remarks) {
        this.r_dh_remarks = r_dh_remarks;
    }
}
